﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class backgroundDebugging : MonoBehaviour
{
    Text t;
    public inventoryControlManager bg;
    // Start is called before the first frame update
    void Start()
    {
        t = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        bg = GameObject.FindObjectOfType<inventoryControlManager>();
        t.text = "" + bg.inventoryController.gameObject.name;
        if(GameObject.FindGameObjectWithTag("InventoryController")!=null)
        {
            Debug.Log("Background Exists");
        }
        else
        {
            Debug.Log("Background Does not Exist");
        }
    }
}
