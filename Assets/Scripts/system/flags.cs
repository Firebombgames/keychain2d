﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flags : MonoBehaviour
{
    public Dictionary<string, bool> flag;
    // Start is called before the first frame update
    void Start()
    {
        flag = new Dictionary<string, bool>();

        //initialize hard coded flags here
        addFlag("test");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void addFlag(string name)
    {
        if (!flag.ContainsKey(name))
        {
            flag.Add(name, false);
        }
    }
}
