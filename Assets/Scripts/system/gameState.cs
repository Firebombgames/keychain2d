﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class gameState
{
    public static state currentState = state.playing;
}

public enum state
{
    playing,
    inInventory,
    inCutscene,
    inputLocked
}
