﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anchorUIToWorld : MonoBehaviour
{
    public Transform anchorPoint;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 anchorPos = anchorPoint.position;
        Vector3 translatedPoint = Camera.main.WorldToScreenPoint(anchorPos);
        transform.position = translatedPoint;
    }
}
