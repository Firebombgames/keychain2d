﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class dialogTest : MonoBehaviour
{
    public TextMeshProUGUI t2;
    public Text t1;
    // Start is called before the first frame update
    void Start()
    {
        t1 = GetComponent<Text>();
        t2 = GameObject.FindObjectOfType<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        t1.text = t2.text;
    }
}
