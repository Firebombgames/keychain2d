﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class sceneInfo : MonoBehaviour
{
    public string currentScene;
    public string NextScene;
    public string previousScene;
    public Scene loadedScene;
    public bool isLoading = false;
    public bool sceneLoaded = false;
    public float loadingProgress;
    public Dictionary<string, string> scenes;
    public Dictionary<string, Vector2> entryPoints;
    // Start is called before the first frame update
    void Start()
    {
        scenes = new Dictionary<string, string>();
        entryPoints = new Dictionary<string, Vector2>();
    }

    // Update is called once per frame
    void Update()
    {
        currentScene = SceneManager.GetActiveScene().name;
    }

    public int getLoadPercent()
    {
        if (isLoading)
            return (int)loadingProgress * 100;
        else
            return 0;
    }

    
}
