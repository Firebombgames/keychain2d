﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stopFlip : MonoBehaviour
{
    public Transform parent;
    // Start is called before the first frame update
    void Start()
    {
        parent = transform.parent.transform;
    }

    // Update is called once per frame
    void Update()
    {
       if(parent.localScale.x == -1)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
       else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }
}
