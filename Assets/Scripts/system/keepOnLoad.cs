﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keepOnLoad : MonoBehaviour
{
    public static keepOnLoad instance;
    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        if(instance==null)
        {
            instance = this;
        }
        if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }
}
