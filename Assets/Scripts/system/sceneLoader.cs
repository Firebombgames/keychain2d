﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneLoader : MonoBehaviour
{
    public sceneInfo info;
    public bool autoLoadScene;
    public string LoadingScreen;
    // Start is called before the first frame update
    void Start()
    {
        GameObject.FindObjectOfType<sceneInfo>();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // Update is called once per frame
    void Update()
    {
        if(autoLoadScene)
        {
            if(info.sceneLoaded)
            {
                makeLoadedSceneActive();
            }
        }
        if(info.isLoading)
        {
            if(info.getLoadPercent()>=100)
            {
                info.isLoading = false;
            }
        }
        Debug.Log(SceneManager.GetActiveScene().name);
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        info.loadedScene = scene;
        Debug.Log("loaded " + scene.name);
        if(info.loadedScene.name==LoadingScreen)
        {
            SceneManager.LoadSceneAsync(info.NextScene);
        }
    }

    public void startAsyncLoad(string scene)
    {
        info.NextScene = scene;
        info.isLoading = true;
        SceneManager.LoadScene(LoadingScreen);
    }

    public void startAsyncLoad()
    {
        info.isLoading = true;
        SceneManager.LoadScene(LoadingScreen);
    }

    public void loadScene(string scene)
    {
        info.NextScene = scene;
        info.previousScene = info.currentScene;
        info.currentScene = scene;
        info.NextScene = "";
        SceneManager.LoadScene(LoadingScreen);
    }

    public void loadScene()
    {
        info.previousScene = info.currentScene;
        info.currentScene = info.NextScene;
        info.NextScene = "";
        SceneManager.LoadScene(LoadingScreen);
    }

    public void makeLoadedSceneActive()
    {
        if(info.sceneLoaded)
        {
            info.previousScene = SceneManager.GetActiveScene().name;
            info.isLoading = false;
            info.sceneLoaded = false;
            info.NextScene = "";
            SceneManager.LoadScene(info.NextScene);
            info.currentScene = SceneManager.GetActiveScene().name;
        }
    }
}
