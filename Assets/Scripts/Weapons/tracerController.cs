﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tracerController : MonoBehaviour
{
    public float speed;
    public Rigidbody2D rb;
    public SpriteRenderer sr;
    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * speed;
        sr = GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    { 
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * speed;
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = transform.right * speed;
        if (!sr.isVisible)
        {
            GameObject.FindObjectOfType<tracerStack>().removeTracer(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("hit: " + collision.gameObject.name);
        collision.transform.SendMessage("bulletHit"); //change this to add damage later
        GameObject.FindObjectOfType<tracerStack>().removeTracer(gameObject);

    }
}
