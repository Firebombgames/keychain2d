﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shotgunShellEjector : MonoBehaviour
{
    public GameObject shell;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void eject()
    {
        GameObject g = Instantiate(shell, transform.position, transform.rotation);
        g.GetComponent<Rigidbody2D>().AddForce(transform.up * 300);
    }
}
