﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drawSphereGizmod : MonoBehaviour
{
    public Color c;
    public float size;
    public bool drawRight;
    public Color rightColor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = c;
        Gizmos.DrawSphere(transform.position, size);
        if(drawRight)
        {
            Gizmos.color = rightColor;
            Gizmos.DrawLine(transform.position, transform.position + transform.right);
        }
    }
}
