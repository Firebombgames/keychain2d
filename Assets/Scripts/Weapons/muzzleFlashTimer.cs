﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class muzzleFlashTimer : MonoBehaviour
{
    public float maxTime;
    public float counter;
    public Light2D l;
    public float maxIntensity;

    // Start is called before the first frame update
    void Start()
    {
        counter = maxTime;
    }

    // Update is called once per frame
    void Update()
    {
        l.intensity = Mathf.Lerp(maxIntensity, 0, counter / maxTime);
        Debug.Log(counter / maxTime);
        if (counter > maxTime)
        {
            counter = maxTime;
        }
        else
        {
            counter += Time.deltaTime;
        }
    }

    public void drawLight()
    {
        l.intensity = maxIntensity;
        counter = 0;
    }
}
