﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot : MonoBehaviour
{
    public tracerStack stack;
    public List<Transform> shotgunShootpoints;
    public Transform handgunShootpoint;
    public float bulletSpeed;
    public muzzleFlashTimer handgunFlash;
    public muzzleFlashTimer shotgunFlash;
    public AudioSource handgunSound;
    public AudioSource shotgunSound;
    // Start is called before the first frame update
    void Start()
    {
        stack = GameObject.FindObjectOfType<tracerStack>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void _shoot(int weapon)
    {
        if(weapon==1)
        {
            GameObject g = stack.getTracer();
            g.SetActive(true);
            g.GetComponent<tracerController>().speed = transform.localScale.x * bulletSpeed;
            g.transform.position = handgunShootpoint.position;
            g.transform.rotation = handgunShootpoint.rotation;
            handgunFlash.drawLight();
            handgunSound.Play();
        }
        else
        {
            if(weapon==4)
            {
                foreach(Transform s in shotgunShootpoints)
                {
                    GameObject g = stack.getTracer();
                    g.SetActive(true);
                    g.GetComponent<tracerController>().speed = transform.localScale.x * bulletSpeed;
                    g.transform.position = s.position;
                    g.transform.rotation = s.rotation;
                    shotgunFlash.drawLight();
                    shotgunSound.Play();
                }
            }
        }
    }
}
