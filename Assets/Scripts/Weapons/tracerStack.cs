﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tracerStack : MonoBehaviour
{
    public List<GameObject> tracers;
    public GameObject tracerPrefab;
    public Transform tracerContainer;
    public int desiredTracers = 15;
    // Start is called before the first frame update
    void Start()
    {
            tracers = new List<GameObject>();
        for (int i = 0; desiredTracers > 0; desiredTracers--)
        {
            GameObject g = GameObject.Instantiate(tracerPrefab, tracerContainer);
            tracers.Add(g);
            g.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject getTracer()
    {
        for(int i=0;i<tracers.Count;i++)
        {
            if(tracers[i].activeSelf==false)
            {
                return tracers[i];
            }
        }
        return null;
    }

    public void removeTracer(GameObject t)
    {
        t.SetActive(false);
    }
}
