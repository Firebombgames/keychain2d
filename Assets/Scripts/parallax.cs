﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class parallax : MonoBehaviour
{
    public float scale = 1;
    public playerController player;
    public float xDifference;
    public float xStart;
    Transform playerTrans;
    Vector2 MeStart;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindObjectOfType<playerController>();
        playerTrans = player.transform;
        xStart = playerTrans.position.x;
        MeStart = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        xDifference = playerTrans.position.x - xStart;
        transform.position = MeStart + new Vector2(xDifference * scale , 0);
    }
}
