﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "item" , menuName ="Inventory/item" , order = 1)]
public class item : ScriptableObject
{
    public Sprite image;
    public string _name;
    public string description;
    public bool equippable;
    public bool droppable;
    public useText _useText;
    public bool stackable;
}

public enum itemType
{
    key,
    weapon,
    ammo,
    healing,
}

public enum useText
{
    use,
    equip,
    reload,
    none
}
