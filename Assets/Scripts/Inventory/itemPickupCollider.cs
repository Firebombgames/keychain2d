﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemPickupCollider : MonoBehaviour
{
    public GameObject itemToPickUp;
    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(itemToPickUp==null)
        {
            itemToPickUp = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(itemToPickUp==collision.gameObject)
        {
            itemToPickUp = null;
        }
    }
}
