﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class itemSlot : MonoBehaviour
{
    public itemData containedItem;
    public Image displayImage;
    public Text countText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (containedItem != null)
        {
            displayImage.sprite = containedItem.i.image;
            if(containedItem.i.stackable)
            {
                countText.text ="" + containedItem.count;
            }
            else
            {
                countText.text = "";
            }
        }
        else
        {
            //blank sprite here
        }
    }
}
