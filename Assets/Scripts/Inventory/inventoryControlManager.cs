﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class inventoryControlManager : MonoBehaviour
{
    [SerializeField]
    public List<itemData> items;
    public GameObject[] slots;
    public int displaying = 0;
    public Text descriptionText;
    public GameObject activeSlot;
    public int activeSlotID = 2;
    public AudioSource moveAudio;
    public AudioSource selectAudio;
    public GameObject useEquip;
    public GameObject drop;
    public Animator inventoryController;
    public bool inventoryOpen;
    public selectedInterface selected = selectedInterface.items;
    // Start is called before the first frame update
    void Start()
    {
        findSlots();
        updateSlots();
    }

    // Update is called once per frame
    void Update()
    {
        findSlots();
        //process opening the inventory
        if(Input.GetKeyDown(KeyCode.I))
        {
            if (!inventoryOpen)
            {
                inventoryOpen = true;
                gameState.currentState = state.inInventory;
            }
            else
            {
                inventoryOpen = false;
                gameState.currentState = state.playing;

            }
        }
        inventoryController.SetBool("inventoryOpen", inventoryOpen);

        if (inventoryOpen)
        {
            //process input
            if (Input.GetKeyDown(KeyCode.D) && selected==selectedInterface.items)
            {
                displaying++;
                if (displaying >= items.Count)
                {
                    displaying = 0;
                }
                moveAudio.Play();
                updateSlots();
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.A) && selected == selectedInterface.items)
                {
                    displaying--;
                    if (displaying < 0)
                    {
                        displaying = items.Count - 1;
                    }
                    moveAudio.Play();
                    updateSlots();
                }
            }

            itemSlot active = activeSlot.GetComponent<itemSlot>();

            //make sure we arent selecting something that cant be used
            if(!active.containedItem.i.droppable && selected==selectedInterface.drop)
            {
                selected = selectedInterface.items;
            }
            if (active.containedItem.i._useText == useText.none && selected == selectedInterface.equipUse)
            {
                selected = selectedInterface.items;
            }

            switch (selected)
            {
                case selectedInterface.items:
                    if (Input.GetKeyUp(KeyCode.S))
                    {
                        if (active.containedItem.i._useText!=useText.none)
                        {
                            selected = selectedInterface.equipUse;
                            moveAudio.Play();
                        }
                        else
                        {
                            if (active.containedItem.i.droppable)
                            {
                                selected = selectedInterface.drop;
                                moveAudio.Play();
                            }
                        }
                    }

                    break;
                case selectedInterface.equipUse:
                    useEquip.GetComponent<Outline>().effectColor = Color.red;
                    if (Input.GetKeyUp(KeyCode.W))
                    {
                        selected = selectedInterface.items;
                        useEquip.GetComponent<Outline>().effectColor = Color.black;
                        moveAudio.Play();
                    }
                    else
                    {
                        if (Input.GetKeyUp(KeyCode.S))
                        {
                            if (active.containedItem.i.droppable)
                            {
                                selected = selectedInterface.drop;
                                moveAudio.Play();
                                useEquip.GetComponent<Outline>().effectColor = Color.black;
                            }
                        }
                    }
                    break;
                case selectedInterface.drop:
                    drop.GetComponent<Outline>().effectColor = Color.red;
                    if (Input.GetKeyUp(KeyCode.W))
                    {
                        if (active.containedItem.i._useText != useText.none)
                        {
                            selected = selectedInterface.equipUse;
                            drop.GetComponent<Outline>().effectColor = Color.black;
                            moveAudio.Play();
                        }
                        else
                        {
                            selected = selectedInterface.items;
                            drop.GetComponent<Outline>().effectColor = Color.black;
                            moveAudio.Play();
                        }
                    }
                    break;
            }

            //process displaying to make sure it stays in range



            if (active.containedItem != null)
            {
                descriptionText.text = active.containedItem.i.description;
            }

            //process the use box
            if (active.containedItem.i.droppable)
            {
                drop.GetComponent<Text>().color = new Color(1, 1, 1, 1);
            }
            else
            {
                drop.GetComponent<Text>().color = new Color(1, 1, 1, 0);
            }
            Text uText = useEquip.GetComponent<Text>();
            switch (active.containedItem.i._useText)
            {
                case useText.equip:
                    uText.text = "Equip";
                    useEquip.GetComponent<Text>().color = new Color(1, 1, 1, 1);
                    break;
                case useText.reload:
                    uText.text = "Reload";
                    useEquip.GetComponent<Text>().color = new Color(1, 1, 1, 1);
                    break;
                case useText.use:
                    uText.text = "Use";
                    useEquip.GetComponent<Text>().color = new Color(1, 1, 1, 1);
                    break;
                case useText.none:
                    uText.text = " ";
                    useEquip.GetComponent<Text>().color = new Color(1, 1, 1, 0);
                    break;
            }
        }
    }

    public void findSlots()
    {
        if (slots == null || slots[0] == null)
        {
            slots = new GameObject[5];
            for (int i = 1; i <= 5; i++)
            {
                slots[i-1] = GameObject.Find("itemSlot" + i);
            }
        }

        moveAudio = GameObject.Find("moveAudio").GetComponent<AudioSource>();
        selectAudio = GameObject.Find("selectAudio").GetComponent<AudioSource>();
        activeSlot = slots[activeSlotID];

        inventoryController = GameObject.FindGameObjectWithTag("InventoryController").GetComponent<Animator>();

        useEquip = GameObject.Find("Equip_use");
        drop = GameObject.Find("Drop");
        descriptionText = GameObject.Find("ItemInfo").GetComponent<Text>();
    }

    public void updateSlots()
    {
       
        for (int i = 0; i < slots.Length; i++)
        {
            int actual_i = 0;
            if (i + displaying >= items.Count)
            {
                actual_i = i + displaying - items.Count;
                while(actual_i >= items.Count)
                {
                    actual_i -= items.Count;
                }
            }
            else
            {
                actual_i = i + displaying;
            }
            //set each item slot
            itemSlot slot = slots[i].GetComponent<itemSlot>();
            slot.containedItem = items[actual_i];

        }
    }
}

[System.Serializable]
public class itemData
{
    public item i;
    public int count;
}

public enum selectedInterface
{
    notes,
    map,
    items,
    equipUse,
    drop
}

