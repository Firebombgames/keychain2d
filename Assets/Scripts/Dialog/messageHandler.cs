﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class messageHandler : MonoBehaviour
{
    public conversation currentConversation;
    public bool showingMessage = false;
    public int onMessage;
    TextMeshProUGUI text;
    public float messageShowTime = 3f;
    float counter;
    public flags flag;

    // Start is called before the first frame update
    void Start()
    {
        text = GameObject.FindObjectOfType<TextMeshProUGUI>();
        flag = GameObject.FindObjectOfType<flags>();
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            if(text==null)
            {
                text = GameObject.FindObjectOfType<TextMeshProUGUI>();
            }
            if(flag==null)
            {
                flag = GameObject.FindObjectOfType<flags>();
            }
        }
        catch
        {

        }
        if(showingMessage)
        {
            //this is a message that waits for player input
            if (playerInput.use == KeyState.up && currentConversation.messages[onMessage].waitForPlayer == true)
            {
                processMessage();               
            }
            else
            {
                //this is a time based message
                if (currentConversation.messages[onMessage].waitForPlayer == false)
                {
                    if (counter >= messageShowTime)
                    {
                        counter -= messageShowTime;
                        processMessage();
                       
                    }
                    counter += Time.deltaTime;
                }
            }
            if (currentConversation != null)
            {
                text.text = currentConversation.messages[onMessage].text;
            }
        }
    }

    public void MessageFinished()
    {
        showingMessage = false;
        onMessage = 0;
        currentConversation = null;
        text.text = "";
    }

    public void processMessage()
    {
        if (onMessage + 1 < currentConversation.messages.Count)
        {
            nextMessage();
        }
        else
        {
            MessageFinished();
        }
    }

    public void nextMessage()
    {
        onMessage++;
        if(currentConversation.messages[onMessage].triggerFlag)
        {
            //update the flag here
            flag.flag[currentConversation.messages[onMessage].flagToTrigger] = true;
        }
        if (currentConversation.messages[onMessage].useAudio)
        {
            AudioSource voiceSource = GameObject.Find("voicePlayer").GetComponent<AudioSource>();
            voiceSource.clip = currentConversation.messages[onMessage].audClip;
            voiceSource.Play();
        }
    }

    public void showMessage(conversation con)
    {
        currentConversation = con;
        showingMessage = true;
        if (currentConversation.messages[onMessage].useAudio)
        {
            AudioSource voiceSource = GameObject.Find("voicePlayer").GetComponent<AudioSource>();
            voiceSource.clip = currentConversation.messages[onMessage].audClip;
            voiceSource.Play();
        }
    }   
}
