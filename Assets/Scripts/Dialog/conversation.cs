﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "message", menuName = "dialog/conversation", order = 1)]
public class conversation : ScriptableObject
{
    public List<message> messages;
    public bool requireFlag;
    public string flagNeeded;
}
