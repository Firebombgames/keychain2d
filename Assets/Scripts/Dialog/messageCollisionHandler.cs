﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class messageCollisionHandler : MonoBehaviour
{
    public bool requireInput = false;
    public bool triggered = false;
    public GameObject messageDisplayIndicator;
    public GameObject objectEntered;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        messageDisplayIndicator.SetActive(objectEntered);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {                
        if(collision.tag=="conversation" && !collision.GetComponent<onCollisionMessage>().requireInput)
        {
            collision.SendMessage("showMessage");
        }
        //set the object entered so we can enable the thing
        if(collision.tag=="conversation")
        {
            gameManager.useReserved = true;
            if (objectEntered == null)
            {
                objectEntered = collision.gameObject;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "conversation" && !collision.GetComponent<onCollisionMessage>().requireInput)
        {
            collision.SendMessage("showMessage");
            objectEntered = null;
        }
        else
        {
            if (collision.tag == "conversation" && playerInput.use == KeyState.up)
            {
                collision.SendMessage("showMessage");
                objectEntered = null;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(objectEntered==collision.gameObject)
        {
            gameManager.useReserved = false;
            objectEntered = null;
        }
    }
}
