﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class messageItem 
{
    public string text;
    public string flag;
    public bool requireFlag;
    public bool waitForPlayer;

    public messageItem(string _text, string _flag, bool _requireFlag, bool _waitForPlayer)
    {
        text = _text;
        flag = _flag;
        requireFlag = _requireFlag;
        waitForPlayer = _waitForPlayer;
    }

    public messageItem(string _text, bool _waitForPlayer)
    {
        text = _text;
        waitForPlayer = _waitForPlayer;
    }
}
