﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="message",menuName ="dialog/message",order=1)]
public class message : ScriptableObject
{
    public string text;    
    public bool waitForPlayer;
    public bool triggerFlag;
    public string flagToTrigger;
    public bool useAudio;
    public AudioClip audClip;
}
