﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onCollisionMessage : MonoBehaviour
{
    public bool onlyOnce;
    public bool triggered;
    public conversation conversation;
    public messageHandler system;
    public bool requireInput;
    public flags flag;
    // Start is called before the first frame update
    void Start()
    {
        system = GameObject.FindObjectOfType<messageHandler>();
        flag = GameObject.FindObjectOfType<flags>();
    }

    // Update is called once per frame
    void Update()
    {
        if(system==null)
        {
            system = GameObject.FindObjectOfType<messageHandler>();
        }
        if(flag==null)
        {
            flag = GameObject.FindObjectOfType<flags>();
        }
    }

    public void showMessage()
    {
        if (!system.showingMessage)
        {
            if (!triggered)
            {
                if (conversation.requireFlag == false || flag.flag[conversation.flagNeeded] == true)
                {
                    triggered = true;
                    system.showMessage(conversation);
                }

            }
            else
            {
                if (!onlyOnce)
                {
                    if (conversation.requireFlag == false || flag.flag[conversation.flagNeeded] == true)
                    {
                        triggered = true;
                        system.showMessage(conversation);
                    }
                }
            }
        }

    }
}
