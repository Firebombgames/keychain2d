﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trailDeactivate : MonoBehaviour
{
    public TrailRenderer t;
    public float timeT;
    public float counter;
    // Start is called before the first frame update
    void Start()
    {
        t = GetComponent<TrailRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(counter>=timeT)
        {
            t.emitting = false;
        }
        else
        {
            t.emitting = true;
            counter += Time.deltaTime;
        }
    }

    public void startTrail()
    {
        counter = 0;
    }
}
