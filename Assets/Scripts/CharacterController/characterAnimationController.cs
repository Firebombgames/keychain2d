﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterAnimationController : MonoBehaviour
{
    public Animator anim;
    public int weapon;
    public bool pickup = false;
    public bool reload = false;
    public bool hit = false;
    public Transform parent;


    //temporary variables for testing
    public int hp = 5;
    public Rigidbody2D rb;
    public float moveSpeed = 100;

    public GameObject gun;
    public GameObject pipe;
    public GameObject knife;
    public GameObject shotGun;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        parent = transform.parent;

        //temporary for testing
        rb = parent.gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameState.currentState == state.playing)
        {
            anim.SetBool("walk", ((playerInput.right == KeyState.held || playerInput.left == KeyState.held) ? true : false));
            anim.SetBool("run", (playerInput.right == KeyState.held && playerInput.run == KeyState.held || playerInput.left == KeyState.held && playerInput.run == KeyState.held) ? true : false);

            if (playerInput.left == KeyState.held && !anim.GetBool("charging"))
            {
                if (parent.transform.localScale.x < 0)
                {

                }
                else
                {
                    parent.transform.localScale = new Vector3(parent.transform.localScale.x * -1, parent.transform.localScale.y, parent.transform.localScale.z);
                }
            }
            else
            {
                if (playerInput.right == KeyState.held && !anim.GetBool("charging"))
                {
                    if (parent.transform.localScale.x > 0)
                    {

                    }
                    else
                    {
                        parent.transform.localScale = new Vector3(parent.transform.localScale.x * -1, parent.transform.localScale.y, parent.transform.localScale.z);
                    }
                }
            }
            if (playerInput.attack == KeyState.up)
            {
                anim.SetBool("charging", false);
            }
            if (playerInput.attack == KeyState.up && !hit && !pickup && !reload)
            {
                anim.SetTrigger("attack");
            }
            else
            {
                if (playerInput.attack == KeyState.pressed && !hit && !pickup && !reload && weapon == 2)
                {
                    anim.SetTrigger("attack");
                }
            }
            if (playerInput.attack == KeyState.held && weapon == 2 || playerInput.attack == KeyState.held && weapon == 3)
            {
                anim.SetBool("charging", true);
            }
            else
            {
                anim.SetBool("charging", false);
            }
            switch (weapon)
            {
                case 0:
                    anim.SetBool("knife", false);
                    anim.SetBool("handgun", false);
                    anim.SetBool("meleeWeapon", false);
                    gun.SetActive(false);
                    pipe.SetActive(false);
                    knife.SetActive(false);
                    break;
                case 1:
                    anim.SetBool("knife", false);
                    anim.SetBool("handgun", true);
                    anim.SetBool("meleeWeapon", false);
                    gun.SetActive(true);
                    pipe.SetActive(false);
                    knife.SetActive(false);
                    shotGun.SetActive(false);
                    break;
                case 2:
                    anim.SetBool("knife", false);
                    anim.SetBool("handgun", false);
                    anim.SetBool("meleeWeapon", true);
                    gun.SetActive(false);
                    pipe.SetActive(true);
                    knife.SetActive(false);
                    shotGun.SetActive(false);
                    break;
                case 3:
                    anim.SetBool("knife", true);
                    anim.SetBool("handgun", false);
                    anim.SetBool("meleeWeapon", false);
                    gun.SetActive(false);
                    pipe.SetActive(false);
                    knife.SetActive(true);
                    shotGun.SetActive(false);
                    break;
                case 4:
                    anim.SetBool("knife", false);
                    anim.SetBool("handgun", false);
                    anim.SetBool("meleeWeapon", false);
                    anim.SetBool("shotgun", true);
                    gun.SetActive(false);
                    pipe.SetActive(false);
                    knife.SetActive(false);
                    shotGun.SetActive(true);
                    break;
                default:
                    anim.SetBool("handgun", false);
                    anim.SetBool("meleeWeapon", false);
                    gun.SetActive(false);
                    pipe.SetActive(false);
                    weapon = 0;
                    knife.SetActive(false);
                    shotGun.SetActive(false);
                    break;
            }
            if (Input.GetKeyUp(KeyCode.Q))
            {
                weapon++;
            }
            if (playerInput.reload == KeyState.up)
            {
                anim.SetTrigger("reload");
                reload = true;
            }
            if (playerInput.use == KeyState.up && !gameManager.useReserved)
            {
                anim.SetTrigger("pickup");
                pickup = true;
            }

            //this code is temporary for testing
            //rb.velocity = new Vector2(Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime, 0);
            //if(Input.GetKey(KeyCode.LeftShift))
            //{
            //rb.velocity = new Vector2(Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime * 5, rb.velocity.y);
            //}
        }
    }

    public void backTriggerFinished()
    {
        if(pickup)
        {
            itemPickupCollider ic = GameObject.FindObjectOfType<itemPickupCollider>();
            if (ic.itemToPickUp != null)
            {
                inventoryControlManager icm = GameObject.FindObjectOfType<inventoryControlManager>();
                if(!icm.items.Contains(ic.itemToPickUp.GetComponent<itemPickup>().i))
                {
                    icm.items.Add(ic.itemToPickUp.GetComponent<itemPickup>().i);
                    //display picked up item here
                    GameObject.Destroy(ic.itemToPickUp);
                }
                else
                {
                    icm.items[icm.items.IndexOf(ic.itemToPickUp.GetComponent<itemPickup>().i)].count++;
                    //display picked up item here
                    GameObject.Destroy(ic.itemToPickUp);
                }
            }
        }
        reload = false;
        pickup = false;
    }

    public void hitFinished()
    {
        hit = false;
    }


    public void startTrail()
    {
        pipe.SendMessage("startTrail");
    }
    

    public void ejectShell()
    {
        GameObject.FindObjectOfType<shotgunShellEjector>().eject();
    }

    public void shoot()
    {
        transform.parent.SendMessage("_shoot", weapon);
    }
}
