﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class playerInput
{
    public static keyBindings binding=new keyBindings(true);
    public static KeyState right;
    public static KeyState left;
    public static KeyState up;
    public static KeyState use;
    public static KeyState attack;
    public static KeyState reload;
    public static KeyState run;

    

    // Update is called once per frame
    public static void Update()
    {
        updateKeystate(ref right, binding.right);
        updateKeystate(ref left, binding.left);
        updateKeystate(ref up, binding.up);
        updateKeystate(ref use, binding.use);
        updateKeystate(ref attack, binding.attack);
        updateKeystate(ref reload, binding.reload);
        updateKeystate(ref run, binding.sprint);
    }

    static void updateKeystate(ref KeyState ks, KeyCode kc)
    {
        if(Input.GetKeyDown(kc))
        {
            ks = KeyState.pressed;
        }
        else
        {
            if(Input.GetKey(kc))
            {
                ks = KeyState.held;
            }
            else
            {
                if(Input.GetKeyUp(kc))
                {
                    ks = KeyState.up;
                }
                else
                {
                    ks = KeyState.none;
                }
            }
        }
    }
}

[System.Serializable]
public struct keyBindings
{
    public KeyCode right;
    public KeyCode left;
    public KeyCode up;
    public KeyCode use;
    public KeyCode attack;
    public KeyCode reload;
    public KeyCode sprint;

    public keyBindings(bool b)
    {
        right = KeyCode.D;
        left = KeyCode.A;
        up = KeyCode.W;
        use = KeyCode.F;
        attack = KeyCode.E;
        reload = KeyCode.R;
        sprint = KeyCode.LeftShift;
    }
}

public enum KeyState
{
    up,
    held,
    pressed,
    none
}