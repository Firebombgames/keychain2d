﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{
    public float moveSpeed;
    public Vector2 iVelocity;
    public Vector2 aVelocity;
    public Vector2 force;
    public bool slope;
    public float slopeRot;
    public string slopeTag;
    public Vector2 maxVelocity;
    public Vector2 minVelocity;
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameState.currentState == state.playing)
        {
            if(rb.IsSleeping())
                rb.WakeUp();
            aVelocity = rb.velocity;
            playerInput.Update();

            //regular horizontal movement
            iVelocity = aVelocity;
            force = Vector2.zero;

            //set the force value
            if (playerInput.right == KeyState.held)
            {
                force.x = 1;
            }
            else
            {
                if (playerInput.left == KeyState.held)
                {
                    force.x = -1;
                }
                else
                {
                    //lerp twords 0
                    iVelocity = new Vector2(Mathf.Lerp(iVelocity.x, 0, Time.deltaTime), iVelocity.y);
                }
            }
            int runVal = 1;
            //is the player sprinting?
            if (playerInput.run == KeyState.held)
            {
                force.x *= 5;
                runVal = 5;
            }
            if (slope)
            {
                int slopeInput = (int)force.x / 5;
                if (slopeRot > 0)
                {
                    force = new Vector2(1 * slopeInput, 1) + new Vector2(1, 1) * force;
                    force.Normalize();
                }
            }

            iVelocity += force * moveSpeed * Time.deltaTime;
            iVelocity = new Vector2(Mathf.Clamp(iVelocity.x, minVelocity.x * runVal, maxVelocity.x * runVal), Mathf.Clamp(iVelocity.y, minVelocity.y, maxVelocity.y));
            rb.velocity = iVelocity;
        }
        else
        {
            rb.Sleep();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag==slopeTag)
        {
            slope = true;
            slopeRot = collision.transform.rotation.eulerAngles.z;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == slopeTag)
        {
            slope = false;
            slopeRot = 0;
        }
    }
}
