﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class splashScreen : MonoBehaviour
{
    public string sceneToLoad;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FinishedAnimation()
    {
        GameObject.FindObjectOfType<sceneLoader>().info.NextScene=sceneToLoad;
        GameObject.FindObjectOfType<sceneLoader>().startAsyncLoad();
    }
}
