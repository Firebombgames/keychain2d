﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScreen : MonoBehaviour
{
    public string startScene;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        playerInput.Update();
        if(playerInput.use == KeyState.pressed)
        {
            OnStart();
        }
    }

    public void OnQuit()
    {
        Debug.Log("quit");
        Application.Quit();
    }

    public void OnStart()
    {
        Debug.Log("start");
        GameObject.FindObjectOfType<sceneLoader>().startAsyncLoad(startScene);
        GameObject.FindObjectOfType<keepOnLoad>().GetComponent<AudioSource>().Stop();
    }
}
