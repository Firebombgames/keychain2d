﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class introTextController : MonoBehaviour
{
    public Text centerText;
    public Text dialogText;
    public Text keychainText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void firebombPresents()
    {
        centerText.text = "Firebomb Games Presents";
    }

    public void UnityPowered()
    {
        centerText.text = "A Powered by Unity Game";
    }

    public void ExIntro()
    {
        dialogText.text = "Ethan, this Conversation is over.";
    }

    public void Ethan1()
    {
        dialogText.text = "Just...";
    }

    public void Ethan2()
    {
        dialogText.text = "Just Give me 20 Minutes, I can be there in 20 minutes.";
    }

    public void Ex1()
    {
        dialogText.text = "Theres nothing left to talk about. I cant come in second place to a book anymore.. I wont.";
    }

    public void ethan3()
    {
        dialogText.text = "Your not Sec.. Look just wait for me okay? I'm at my car right n.. Shit where are my keys...";
    }

    public void Ex2()
    {
        dialogText.text = "Its too late. Goodbye Ethan";
    }

    public void Ethan4()
    {
        dialogText.text = "Wait!";
    }

    public void clearDialog()
    {
        dialogText.text = "";
    }

    public void clearCenter()
    {
        centerText.text = "";
    }

    
}
